package briatore.exkafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
public class ExKafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExKafkaApplication.class, args);
	}

	@Bean
	public NewTopic novoTopico(){
		return TopicBuilder.name("novo_topico").build();
	}

	@Bean
	public CommandLineRunner runner(KafkaTemplate<String, String> template){
		return args -> {
			for (int i = 0; i < 1_000_000; i++) {
				template.send("novo_topico", "Mensagem " + i + " produzida pelo Spring Boot");
			}
		};
	}

	@KafkaListener(id ="meu_consumidor",  topics = "novo_topico")
	public void consuming(String message){
		System.out.println("Consumindo mensagem: " + message);
	}



}
